from discord.ext import commands
import discord
import asyncio
import requests

# Discord
TOKEN = 'NDU4NDIzMDAyMDAyODgyNTYw.Dgv1KA.h96K9N9me9Bjkv3G2W4t2ozX4Mk'

# Google
API_KEY = 'AIzaSyCH--oNzhu7NkkdQ-sV70p03w_otqhburw'


def get_youtube_data(song_name):
    yt_url = 'https://www.googleapis.com/customsearch/v1'
    parameters = {'q': song_name.replace(' ', '+'),
                  'cx': '004703667314782214122:nxjl3asw2dc',
                  'key': API_KEY,
                  'num': 1}

    get_request = requests.get(yt_url, params=parameters)
    return get_request.json()


def get_youtube_url(json):
    url = json['items'][0]['link']
    return url


def get_description_from_youtube_data(json):
    title = json['items'][0]['pagemap']['metatags'][0]['title']
    thumbnail = json['items'][0]['pagemap']['videoobject'][0]['thumbnailurl']
    return title, thumbnail


class SongObject:
    def __init__(self, message, song):
        self.title = song
        self.channel = message.channel


class VoiceCommands:
    def __init__(self, client):
        self.bot = client
        self.voice = None
        self.voice_connected = None
        self.music_player = None
        self.music_state = None
        self.queue = asyncio.Queue()
        self.next_song = asyncio.Event()

        client.loop.create_task(self.audio_player_loop())

    async def join_voice_channel(self, channel):
        await self.bot.say(f'Joining {channel}...')
        self.voice = await self.bot.join_voice_channel(channel)
        self.voice_connected = True

    def get_voice_channels(self, ctx):
        voice_channels = []
        server = ctx.message.author.server
        for channel in server.channels:
            if str(channel.type) is 'voice':
                voice_channels.append(channel)
        return voice_channels

    @commands.command(name='join', pass_context=True)
    async def join_voice_channel_command(self, ctx, *args):
        channel = ' '.join(args)
        if self.voice_connected is True and channel is self.voice.channel:
            await self.bot.say('Already in this voice channel...')
            self.voice_connected = False
            return
        elif self.voice_connected is True:
            await self.voice.disconnect()

        if len(channel) == 0:  # No arguments passed through command
            channel = ctx.message.author.voice_channel
            if channel is None:  # Message author currently not in voice channel
                await self.bot.say('You are currently not in a channel...')
                self.voice_connected = False
            else:
                await self.join_voice_channel(channel)
                return
        else:
            voice_channel_list = self.get_voice_channels(ctx)
            for existing_channel in voice_channel_list:
                if channel == existing_channel.name:  # Argument passed through is a valid channel
                    await self.join_voice_channel(existing_channel)
                    return
            else:
                output_message = f'```{channel} does not exist, here are some other options:\n'
                for channel in voice_channel_list:
                    output_message += f'-{channel.name}\n'
                output_message += '```'
                await self.bot.say(output_message)
                self.voice_connected = False

    @commands.command(name='leave')
    async def leave_voice_channel_command(self):
        if self.voice_connected is True:
            await self.voice.disconnect()
            self.voice_connected = False
            self.voice = None
            await self.bot.say('Leaving voice channel...')
        else:
            await self.bot.say('I\'m not in a voice channel...')

    async def play_song(self, song):
        print(song)
        json = get_youtube_data(song.title)
        url = get_youtube_url(json)
        title, thumbnail = get_description_from_youtube_data(json)
        await self.bot.send_message(song.channel, f':musical_note: Currently playing **{title}** :musical_note:\n{thumbnail}')
        self.music_player = await self.voice.create_ytdl_player(url, after=lambda: self.play_next())
        self.music_player.start()

    def play_next(self):
        print('Play next')
        self.bot.loop.call_soon_threadsafe(self.next_song.set)  # Unblocks audio_player_loop

    async def audio_player_loop(self):
        while True:
            self.next_song.clear()
            await self.play_song_controller()
            await self.next_song.wait()  # Waits until next_song is unblocked

    async def play_song_controller(self, song=None):
        print('Playing song controller')
        if song is None:
            if self.music_state == 'paused':
                self.music_player.resume()
            else:
                if self.queue.qsize() > 0:
                    song = await self.queue.get()
                    self.music_state = 'play'
                    await self.play_song(song)
        else:
            self.music_state = 'play'
            await self.play_song(song)

    @commands.command(name='play', pass_context=True)
    async def play_command(self, ctx, *args):
        song = SongObject(ctx.message, ' '.join(args))
        if self.voice_connected is True:
            if len(song.title) == 0:
                await self.play_song_controller(None)
            else:
                await self.play_song_controller(song)
        else:
            await self.bot.say('Music bot is not attached to any voice channel...')

    @commands.command(name='pause')
    async def pause_command(self):
        print('Pausing player')
        if self.music_player.is_playing() is True:
            self.music_player.pause()
            self.music_state = 'paused'

    @commands.command(name='next')
    async def next_command(self):
        if self.music_player is not None and self.queue.qsize() > 0:
            self.music_player.stop()

    @commands.command(name='queue', pass_context=True)
    async def queue_command(self, ctx, *args):
        print(f'Adding {args} to queue...')
        song = SongObject(ctx.message, ' '.join(args))
        if len(song.title) == 0:
            await self.bot.say(self.queue)
        else:
            json = get_youtube_data(song.title)
            title, thumbnail = get_description_from_youtube_data(json)
            await self.queue.put(song)
            await self.bot.say(f':musical_note: Adding **{title}** to queue :musical_note:')


# Initializes bot fully, adds MusicCommand class too
def setup():
    client = commands.Bot(command_prefix='!')
    client.add_cog(VoiceCommands(client))
    return client


bot = setup()
bot.run(TOKEN)
