import requests
import webbrowser

CLIENT_ID = 'e5386af8f40448548acfb7913cc47664'
CLIENT_SECRET = 'f197ab252aa04c62943afe91d46af987'
REDIRECT_URI = 'http://127.0.0.1:3000'


def get_access_token_url():
    parameters = {'client_id': CLIENT_ID,
                  'response_type': 'token',
                  'redirect_uri': REDIRECT_URI,
                  }
    url = 'https://accounts.spotify.com/authorize'

    access_token_request = requests.post(url, params=parameters, )
    return access_token_request.url


def user_playlist_parser(uri):
    split_uri = uri.split(':')
    user_id = split_uri[2]
    playlist_id = split_uri[4]
    return user_id, playlist_id


def song_artist_json_parser(json):
    output_list = []
    for track in json['items']:
        artist = track['track']['artists'][0]['name']
        track_title = track['track']['name']
        output_list.append(f'{track_title} - {artist}')
    return output_list


def get_tracks_from_playlist(uri, access_token):
    user_id, playlist_id = user_playlist_parser(uri)
    url = f'https://api.spotify.com/v1/users/{user_id}/playlists/{playlist_id}/tracks'
    parameters = {'fields': 'items(track(name, artists))'}
    header = {'Authorization': 'Bearer ' + access_token}

    playlist_tracks_request = requests.get(url, params=parameters, headers=header)

    return playlist_tracks_request.json()



