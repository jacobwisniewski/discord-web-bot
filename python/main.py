import discord
from discord.ext import commands
import requests
import asyncio
from copy import deepcopy


# Discord
TOKEN = 'NDU4NDIzMDAyMDAyODgyNTYw.Dgv1KA.h96K9N9me9Bjkv3G2W4t2ozX4Mk'

# Google
API_KEY = 'AIzaSyCmJ4uJgpvJ2jX7Zf-vYVmNvDgbu-ilVgM'

# Spotify
CLIENT_ID = 'e5386af8f40448548acfb7913cc47664'
CLIENT_SECRET = 'f197ab252aa04c62943afe91d46af987'
REDIRECT_URI = 'http://127.0.0.1:3000'


def get_tracks_from_playlist(uri, access_token):
    user_id, playlist_id = user_playlist_parser(uri)
    url = f'https://api.spotify.com/v1/users/{user_id}/playlists/{playlist_id}/tracks'
    parameters = {'fields': 'items(track(name, artists))'}
    header = {'Authorization': 'Bearer ' + access_token}

    playlist_tracks_request = requests.get(url, params=parameters, headers=header)

    return playlist_tracks_request.json()


def get_access_token_url():
    parameters = {'client_id': CLIENT_ID,
                  'response_type': 'token',
                  'redirect_uri': REDIRECT_URI,
                  }
    url = 'https://accounts.spotify.com/authorize'

    access_token_request = requests.post(url, params=parameters, )
    return access_token_request.url


def user_playlist_parser(uri):
    split_uri = uri.split(':')
    user_id = split_uri[2]
    playlist_id = split_uri[4]
    return user_id, playlist_id


def song_artist_json_parser(json):
    output_list = []
    for track in json['items']:
        artist = track['track']['artists'][0]['name']
        track_title = track['track']['name']
        output_list.append(f'{artist} {track_title}')
    return output_list


def get_youtube_data(song_name):
    yt_url = 'https://www.googleapis.com/customsearch/v1'
    parameters = {'q': song_name.replace(' ', '+'),
                  'cx': '004703667314782214122:nxjl3asw2dc',
                  'key': API_KEY,
                  'num': 1}

    get_request = requests.get(yt_url, params=parameters)
    return get_request.json()


def get_youtube_url(json):
    url = json['items'][0]['link']
    return url


def get_description_from_youtube_data(json):
    title = json['items'][0]['pagemap']['metatags'][0]['title']
    thumbnail = json['items'][0]['pagemap']['videoobject'][0]['thumbnailurl']
    return title, thumbnail

def queue_max(queue_length):
    if queue_length > 3:
        return 3
    else:
        return queue_length

def get_parsed_queue_data(queue):
    output_data = '```'
    temp = deepcopy(queue)
    for index in range(0, queue_max(len(queue))):
        song_name = temp.dequeue()
        json = get_youtube_data(song_name)
        title, thumbnail = get_description_from_youtube_data(json)
        output_data += f'{index + 1}. {title}\n'
    if len(queue) > 3:
        output_data += f'Plus {len(queue) - 3} more songs\n```'
    else:
        output_data += '```'

    if len(queue) == 0:
        output_data = 'Nothing in the queue...'
    return output_data


class Queue:
    def __init__(self):
        self.queue = list()

    def __len__(self):
        return len(self.queue)

    def enqueue(self, arg):
        self.queue.append(arg)

    def dequeue(self):
        return self.queue.pop(0)

    def peek(self):
        return self.queue[0]


class MusicCommands:
    def __init__(self, bot):
        self.bot = bot
        self.voice = None
        self.voice_connected = None
        self.player = None
        self.queue = Queue()

    async def create_voice_channel_connection(self, channel):
        self.voice = await self.bot.join_voice_channel(channel)
        self.voice_connected = True

    async def play_song(self, args):
        song = ' '.join(args)
        json = get_youtube_data(song)
        url = get_youtube_url(json)
        title, thumbnail = get_description_from_youtube_data(json)
        await self.bot.say(f':musical_note: Currently playing {title} :musical_note:\n{thumbnail}')
        self.player = await self.voice.create_ytdl_player(url, after=lambda: self.play_next())
        self.player.start()

    def play_next(self):
        asyncio.run_coroutine_threadsafe(self.play_command_controller(), bot.loop())

    async def play_command_controller(self, args=None):
        if args is None:
            # If player was paused, and you want to resume playback ex: !play
            if self.player is not None:
                if self.player.is_playing is False:
                    self.player.resume()
                    return

            if len(self.queue) > 0:
                args = self.queue.dequeue()
            else:
                self.player = None
                return

        # If no player has been initialized, create player and play song
        if self.player is None:
            await self.play_song(args)
        else:
            # If a song is currently playing and you want to override it
            if self.player.is_playing is True:
                self.player.stop()
                await self.play_song(args)
            else:
                # Play a song from queue
                await self.play_song(args)

    @commands.command(name='join', pass_context=True)
    async def join_channel_command(self, ctx):
        channel = ctx.message.author.voice_channel
        if channel is None:
            self.bot.say('You\'re not in a channel...')
            return
        if self.voice_connected is True:
            await self.bot.say('Already in voice channel...')
        else:
            await self.create_voice_channel_connection(channel)

    @commands.command(name='leave')
    async def leave_channel_command(self):
        if self.voice_connected is True:
            await self.voice.disconnect()
            self.voice_connected = False

    @commands.command(name='play')
    async def play_command(self, *args):
        if len(args) == 0:
            args = None
        await self.play_command_controller(args)

    @commands.command(name='pause')
    async def stop_command(self):
        self.player.pause()
        await self.bot.say('Pausing your tunes...')

    @commands.command(name='resume')
    async def resume_command(self):
        self.player.resume()
        await self.bot.say('Resuming your tunes...')

    @commands.command(name='queue')
    async def queue_command(self, *args):
        if len(args) == 0:
            queue_data = get_parsed_queue_data(self.queue)
            await self.bot.say(queue_data)
        else:
            song = ' '.join(args)
            self.queue.enqueue(song)
            json = get_youtube_data(song)
            title, thumbnail = get_description_from_youtube_data(json)
            await self.bot.say(f':musical_note: Adding {title} to the queue :musical_note:')

    @commands.command(name='volume')
    async def volume_command(self, arg):
        self.player.volume = float(arg)
        await self.bot.say(':musical_note: Volume set at {}% :musical_note:'.format(str(100*float(arg))))

    @commands.command(name='next')
    async def next_command(self):
        if len(self.queue) > 0:
            self.player.stop()
            return
        else:
            await self.bot.say('Nothing in the queue...')

    @commands.command(name='spotify', pass_context=True)
    async def spotify_command(self, ctx, arg):
        url = get_access_token_url()
        await self.bot.say(f'Please click on this URL, and then copy-paste the redirected URL to this chat\n'
                           f'{url}')

        access_token = await self.bot.wait_for_message(author=ctx.message.author)
        access_token = access_token.content
        access_token = access_token[36:-34]

        json = get_tracks_from_playlist(arg, access_token)
        playlist = song_artist_json_parser(json)

        for song in playlist:
            self.queue.enqueue(song)
            print(song)

bot = commands.Bot(command_prefix='!')
bot.add_cog(MusicCommands(bot))

bot.run(TOKEN)

